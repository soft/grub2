title-color: "#000044"
desktop-color: "#ffffff"
desktop-image: "background.jpg"
message-color: "#000000"
message-bg-color: "#ffffff"

+ boot_menu {
    left = 15%
    top = 15%
    width = 70%
    height = 40%
    item_color = "#888888"
    selected_item_color = "#000044"
    selected_item_pixmap_style = "star_*.jpg"
    item_height = 16
    item_spacing = 4
    item_padding = 0
    icon_width = 0
    icon_height = 0
    item_icon_space = 0
}

+ progress_bar {
    id = "__timeout__"
    left = 20%
    top = 70%
    width = 80%
    height = 18
    bg_color = "#002266"
    fg_color = "#334488"
    text_color = "#ffffff"
    border_color = "#3344aa"
    show_text = true
    text = "@TIMEOUT_NOTIFICATION_LONG@"
}
